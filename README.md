Cordova plugin wrapper for Config store
=======================================
This wrapper packages and builds the platform-specific modules
offering Config store support for a Cordova-based application.


Documentation
-------------
To add persistent configuraion settings storage support to a
Cordova-based application, issue the following command in the
application folder to install version 0.0.2:
```
cordova plugin add https://bitbucket.org/4s/cdvw-config-store.git#v0.0.2
```

Dependency
----------
There does not exist an explicit dependency on the cordova adaptor(https://bitbucket.org/4s/cordova-adaptor/)
but any use of this project requires the cordova adapter to be 
installed also.

During installation of this project two files are copied into the
cordova infrastructure:

- module-name.txt
- src/android/build-extras.gradle

The gradle file will pull the config-store module (https://bitbucket.org/4s/config-store-android-module)
and module-name file is used by the cordova-adaptor to start the
module with that name from the config-store module.

Versioning
----------
To release a new version use the npm version command, for example to create a new patch version, run:
```bash
npm version patch
```
Then push with --follow-tags parameter

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.


License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0
